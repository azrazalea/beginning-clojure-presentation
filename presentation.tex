\documentclass{beamer}
\title{Beginning Clojure}
\subtitle{from theory to code}
\author{Lily Carpenter}
\institute{https://gitlab.com/azrazalea/}
\mode<presentation> {\usetheme{Dresden}}
\date{}

\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{upquote}

\AtBeginDocument{%
  \def\PYZsq{\textquotesingle}%
}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Summary}
  \tableofcontents
\end{frame}

\section{Clojure in Theory}

\begin{frame}
  \frametitle{Reasons for Clojure}

  \begin{itemize}
  \item{A Lisp}
  \item{for Functional Programming}
  \item{symbiotic with an established Platform}
  \item{designed for Concurrency}
  \end{itemize}

  \url{http://clojure.org/rationale}
\end{frame}

\begin{frame}
  \frametitle{Fundamental Concepts of Clojure}

  \begin{itemize}
  \item{Simplicity}
  \item{Freedom to focus}
  \item{Empowerment}
  \item{Clarity}
  \item{Consistency}
  \end{itemize}
  \textit{The Joy of Clojure} by Michael Fogus
\end{frame}

\begin{frame}
  \includegraphics[keepaspectratio=true,width=.75\paperwidth]{clojure_foundations.png}

  \textit{The Joy of Clojure} figure 1.1
\end{frame}

\section{Development Environments}

\begin{frame}
  \frametitle{Development Environments}

  In order of popularity.

  \begin{itemize}
  \item{Emacs (The one true operating system)[use CIDER]}
  \item{Cursive (IntelliJ plugin now, standalone in the future)}
  \item{Vim (fireplace.vim seems to be the popular plugin)}
  \item{Light Table (Default clojure support?)}
  \end{itemize}

  \url{https://cognitect.wufoo.com/reports/state-of-clojure-2014-results/}
\end{frame}

\begin{frame}
  \frametitle{Emacs}

  \begin{itemize}
  \item{Large learning curve if you haven't used it before.}
  \item{I generally recommend heavy use of emacs if and only if you plan on customizing your own config.}
  \item{Most popular clojure IDE by a large margin.}
  \item{Not specific to clojure.}
  \end{itemize}

  \url{http://www.braveclojure.com/basic-emacs/}
\end{frame}

\begin{frame}
  \frametitle{Cursive}

  \begin{itemize}
  \item{``Just works'', simple to setup and start using.}
  \item{May be better if you want to avoid the emacs learning curve.}
  \item{Narrowly the second most used clojure IDE over vim with fireplace.}
  \item{Not yet a 1. 0 release.}
  \item{IntelliJ is not specific to clojure.}
  \end{itemize}

  \url{https://cursiveclojure.com/}
\end{frame}

\begin{frame}
  \frametitle{Vim}

  \begin{itemize}
  \item{Well supported for vim veterans.}
  \item{Would not recommend personally if you don't already know vim (learn emacs instead).}
  \item{Not specific to clojure.}
  \end{itemize}

  \url{https://github.com/tpope/vim-fireplace}
\end{frame}

\begin{frame}
  \frametitle{Light Table}

  \begin{itemize}
  \item{Another ``just works'' kind of option.}
  \item{Don't have to install anything extra for clojure support (though you'll want a build environment).}
  \item{Not specific to clojure}
  \end{itemize}

  \url{http://docs.lighttable.com/}
\end{frame}

\section{Build Environments}
\begin{frame}
  \frametitle{Build environments}

  \begin{itemize}
  \item{Leiningen (98\% of clojurists use per survey)}
  \item{Boot (cool newcomer to the scene, worth trying out probably)}
  \end{itemize}

  \url{http://leiningen.org/}
  \url{https://github.com/tailrecursion/boot}
\end{frame}

\section{Simple Examples}
\begin{frame}[fragile]
  \frametitle{Hello World}
  \begin{minted}{clojure}
    (ns hello-world.core
      (:gen-class)) ; Generate a java class

    (defn -main
      "I don't do a whole lot ... yet."
      [& args]
      (println "Hello, World!"))
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Thread First Macro}
  \begin{minted}{clojure}
    (-> "a b c d"
        .toUpperCase ; Calling directly to java
        (.replace "A" "X")
        (.split " ")
        first)
  \end{minted}
  \url{http://clojuredocs.org/clojure.core/->}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Thread Last Macro}
  \begin{minted}{clojure}
    (->> (range)
         (map #(* % %)) ; Anonymous function
         (filter even?)
         (take 10)
         (reduce +))
  \end{minted}
  \url{http://clojuredocs.org/clojure.core/->>}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Anonymous Functions}
  \begin{minted}{clojure}
    (fn [x y]
      (+ x y))

    #(+ %1 %2)
    #(+ % %) ; Only takes 1 argument
  \end{minted}
\end{frame}

\section{Resources}
\begin{frame}
  \frametitle{Learning}
  \begin{itemize}
  \item{Clojure for the Brave and True \url{http://www.braveclojure.com/}}
  \item{\textit{The Joy of Clojure} \url{http://www.joyofclojure.com/}}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Reference}
  \begin{itemize}
  \item{Grimoire \url{http://conj.io/}}
  \item{Clojure Docs \url{https://clojuredocs.org/}}
  \item{Cheatsheets \url{https://github.com/jafingerhut/clojure-cheatsheets}}
  \item{Clojure Toolbox(library listing) \url{http://www.clojure-toolbox.com/}}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Practice}
  \begin{itemize}
  \item{4clojure \url{http://www.4clojure.com/}}
  \item{Clojure Koans \url{http://clojurekoans.com/}}
  \item{Exercism(not clojure specific) \url{http://exercism.io/}}
  \end{itemize}
\end{frame}

\section{Questions?}
\begin{frame}
  \begin{center}
    \Huge Questions?
  \end{center}
\end{frame}
\end{document}
